// Це ненадійно, тому що введення даних не обов'язково може здійснюватися за допомогою клавіатури
// Є події input і change спеціально обробки введення. Вони спрацьовують в результаті будь-якого введення, включаючи мишею та розпізнавання мови


const btn = document.querySelectorAll('.btn');

document.addEventListener('keydown',(ev)=>{
  btn.forEach(element => {

    if (element.classList.contains('active')) {
      element.classList.remove('active')
    }
    if (`Key${element.dataset.key}` === ev.code || (element.dataset.key === 'Enter' && ev.code === 'Enter' )) {
      element.classList.add('active')
    }
  });
})

